<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php  print $print["language"] ?>" xml:lang="<?php  print $print["language"] ?>">
  <head>
    <title><?php  print $print["title"] ?></title>
    <?php  print $print["head"] ?>
    <?php  print $print["robots_meta"] ?>
    <?php  print $print["base_href"] ?>
    <?php  print $print["favicon"] ?>
    <?php  print $print["css"] ?>
  </head>
  <body>
     <?php foreach ($print as  $key => $info ): ?>
         <?php print "\"".$key."\",\"".$info."\"<br class='dws'/>"?>
      <?php endforeach; ?>
  </body>
</html>
